#!/usr/bin/env python3
from bs4 import BeautifulSoup as soup
import os
import re
import sys
# from slimit.parser import Parser
# from slimit.visitors.nodevisitor import ASTVisitor

def getAllFiles (sLocation):
    aFiles = []
    for folderName, subfolders, filenames in os.walk(sLocation):
        for filename in filenames:
            filename_full = os.path.join(folderName, filename)
            oFileTpl = (folderName, filename, filename_full)
            aFiles.append[oFileTpl]
    return aFiles

def getProjectRoot(sLocation):
    aFolderPaths = []
    for folderName, subfolders, filenames in os.walk(sLocation):
        aFolderPaths.append(folderName)
    
def getFilesByType(sType, sLocation):
    aFiles = []
    for folderName, subfolders, filenames in os.walk(sLocation):
        for filename in filenames:
            filename_noext, file_extension = os.path.splitext(filename)
            print(file_extension)
            if file_extension == sType:
                filename_full = os.path.join(folderName, filename)
                oFileTpl = (folderName, filename, filename_full)
                aFiles.append(oFileTpl)
    return aFiles

def determineRoute(aControllers):
    counterarray = []
    
def getViewAndController(projectroot, sLocation):
    getProjectRoot(sLocation)
    aViews = getArtifactByType("view.xml", sLocation)
    aControllers = getArtifactByType("controller.js", sLocation)
    aViewCtrl = []
    for view in aViews:
        controller = getCtrlFromView(view)
        # print (controller)
        if controller is not None:
          for ctrl in aControllers:
                if ctrl[3] != None:
                    if ctrl[3] == controller:
                        viewctrl = (view[0],view[1],ctrl[0],ctrl[1], controller)
                        aViewCtrl.append(viewctrl)
                        
    return aViewCtrl

def getArtifactByType(sArtifact, sLocation):
    aFiles = []
    for folderName, subfolders, filenames in os.walk(sLocation):
        for filename in filenames:
            if sArtifact is 'util':
                if "controller.js" not in filename:
                    filename_noext, file_extension = os.path.splitext(filename)
                    if file_extension == ".js":
                        filename_full = os.path.join(folderName, filename)
                        oFileTpl = (folderName, filename, filename_full)
                        aFiles.append(oFileTpl)
            elif sArtifact in filename:
                filename_noext, file_extension = os.path.splitext(filename)
                filename_full = os.path.join(folderName, filename)
                if sArtifact == "view.xml":
                    oFileTpl = (folderName, filename, filename_full)
                if sArtifact == "controller.js":
                    controllerdefname = getControllerDefName(filename_full)
                    oFileTpl = (folderName, filename, filename_full, controllerdefname)
                aFiles.append(oFileTpl)
    return aFiles

def getControllerDefName(file):
    try:
        try:
            controller = open(file, 'r', encoding='utf-8')
            lines = controller.read()
        except:
            controller = open(file)
            lines = controller.read()
            pass
        regex = re.compile(r'extend\([\'\"].*[\'\"],')
        matchstr = regex.search(lines)
        matchstr = matchstr.group()
        regex = re.compile(r'[\'\"].*[\'\"]')
        matchstr = regex.search(matchstr)
        return matchstr.group()[1:-1]
    except Exception:
        print (sys.exc_info())
        return None

def getCtrlFromView(file):
    if "view.xml" in file[1]:
        fileobj = open(file[2])
        bs = soup(fileobj, "lxml")
        # print (bs)
        tagarray = bs.findAll(lambda tag: tag.name == "mvc:view")
        if len(tagarray) == 0:
            tagarray = bs.findAll(lambda tag: tag.name == "core:view")    
        controller = None
        if len(tagarray) > 0:
            try:
                controller = tagarray[0]["controllername"]
            except:
                print (sys.exc_info())
                pass
            return controller
 
def getFunctionsFromJS(file):
    try:
        try:
            fileobj = open(file, 'r', encoding='utf-8')
            # this has to be done as read and readlines cannot be executed on the same object
            # and deepcopy() does not work resulting in serialization error
            fileobj2 = open(file, 'r', encoding='utf-8') 
            filecontent = fileobj.readlines()
            filecontent_full = fileobj2.read()
        except:
            fileobj = open(file)
            filecontent = fileobj.readlines()
            filecontent_full = fileobj.read()
            pass
        regex = re.compile(r'[^{$#()",\}]*[=:]*function')
        fnnames = []
        for line in filecontent:
            try:
                byteline = bytes(line, encoding='ascii')
                decodedLine = byteline.decode("unicode-escape")
                if regex.search(line):
                    split = []
                    if ":" in line:
                        split = line.split(":")
                    if "=" in line:
                        split = line.split("=")
                    byteline = bytes(split[0], encoding='ascii')
                    decodedLine = byteline.decode("unicode-escape").strip(' \t\n\r')
                    decodedLine = decodedLine.replace("var ", '')
                    if decodedLine != 'error' and decodedLine != "success" and decodedLine is not "function" and "function" not in decodedLine and "//" not in decodedLine and "*" not in decodedLine and '"' not in decodedLine and decodedLine is not "press" and "press " not in decodedLine:
                        fnnames.append(decodedLine)
                        try:
                            searchstr = r"((/\*\*)[\s\S]*?(" + re.escape(decodedLine) + r"?\s?\S:))"
                            # print (searchstr)
                            searchstr = r"((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))"
                            comment_regex = re.compile(searchstr)
                            commentstring =  comment_regex.search(filecontent_full)
                            commentstring = commentstring.group()
                            print(commentstring)
                        except Exception:
                            print (sys.exc_info())                
                            pass                        
            except:
                print (sys.exc_info())
                pass
        return fnnames
    except:
        print (sys.exc_info())
        pass
        return []